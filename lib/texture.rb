# -*- encoding: utf-8 -*-

require_relative "texture/metadata"
require_relative "texture/base"
require_relative "texture/tga"

module My
  module Texture
    
    def self.load(driver, r_path, force_sRGB = false)
      OpenGL::Kit::ResourceGroupManager.instance.open(r_path) do |io|
        #x 建立 Loader object
        loader = Object.new
        loader.instance_eval { @driver, @stream = driver, io }
        
        #x 根據副檔名選擇 loader type
        case File.extname(r_path)
        when ".tga" then loader.extend LoadingTGA
        else raise RuntimeError, "Unknown Texture File Format"
        end
        
        #x 初始化並回傳 Texture
        Base.new(driver, loader, force_sRGB)
      end
    end
    
  end
end
