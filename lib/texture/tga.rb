# -*- encoding: utf-8 -*-

module My::Texture
  module LoadingTGA
    include OpenGL::Constants
    
    def read_metadata
      data = @stream.read(18).unpack("CCCx5vvvvCC")
      
      # check image type
      # only RGB and uncompressed image is allowed
      colormap_type, image_type = data[1..2]
      unless (image_type == 2 || image_type == 3) && colormap_type == 0
        raise InvalidPixelFormat, "Invalid image type"
      end
      
      id_length = data[0]
      bytes_per_pixel, alpha_depth = data[7], (data[8] & 0xf)
      width, height = (data[5]-data[3]), (data[6]-data[4])
      
      # check pixel format
      pixel_format = case [bytes_per_pixel, alpha_depth]
                     when [24, 0] then :RGB8
                     when [32, 8] then :ARGB8
                     else raise InvalidPixelFormat, "Invalid pixel format"
                     end
      
      # forward the stream position by 'id_length' bytes
      @stream.seek(id_length, IO::SEEK_CUR)
      
      @metadata = Metadata.new(width, height, 1, pixel_format)
    end
    
    def unpack_pixels
      width, height = @metadata.width, @metadata.height
      bpp = @metadata.number_of_bytes_per_pixel
      row_bytes = bpp * width # no padding
      
      ptr = FFI::MemoryPointer.new(:uint8, row_bytes)
      
      0.upto(height-1) do |y|
        # Read pixels from stream
        row_data = @stream.read(row_bytes)
        ptr.write_string(row_data, row_bytes)
        
        # Upload to GPU
        @driver.glTexSubImage2D GL_TEXTURE_2D, 0, 0, y, width, 1,
                                @metadata.format, @metadata.type, ptr
      end
      
      @driver.glGenerateMipmap(GL_TEXTURE_2D)
      
      ptr.free
    end
    
  end
end
