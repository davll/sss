# -*- encoding: utf-8 -*-

module My::Texture
  class Base
    include OpenGL::Constants
    
    attr_reader :driver, :object
    
    def initialize(driver, loader, force_sRGB)
      @driver = driver
      
      @metadata = loader.read_metadata
      
      # create GL object
      @object = @driver.glGenTextures(1)[0]
      
      # bind state
      @driver.glBindTexture(GL_TEXTURE_2D, @object)
      
      # load texture data
      alloc_texture(force_sRGB)
      loader.unpack_pixels
      
      # clear state
      @driver.glBindTexture(GL_TEXTURE_2D, 0)
    end
    
    def dispose
      @driver.glDeleteTextures(@object)
    end
    
    def width
      @metadata.width
    end
    
    def height
      @metadata.height
    end
    
    def pixel_format
      @metadata.pixel_format
    end
    
    private
    
    # Create OpenGL Texture
    def alloc_texture(force_sRGB)
      @driver.glTexImage2D GL_TEXTURE_2D, 0,
                           @metadata.internal_format(force_sRGB),
                           @metadata.width, @metadata.height, 0,
                           @metadata.format, @metadata.type, nil
    end
    
  end
end
