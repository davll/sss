# -*- encoding: utf-8 -*-

module My::Texture
  class Metadata < Struct.new(:width, :height, :depth, :pixel_format)
    include OpenGL::Constants::GL_VERSION_1_1
    include OpenGL::Constants::GL_VERSION_1_2
    include OpenGL::Constants::GL_VERSION_1_3
    include OpenGL::Constants::GL_VERSION_1_4
    include OpenGL::Constants::GL_VERSION_1_5
    include OpenGL::Constants::GL_VERSION_2_0
    include OpenGL::Constants::GL_VERSION_2_1
    
    # @see #bytes_per_pixel
    BYTES_PER_PIXEL = {
      RGB8: 3,
      ARGB8: 4,
    }
    
    # How many bytes a pixel needs
    def number_of_bytes_per_pixel
      BYTES_PER_PIXEL.fetch(pixel_format)
    end
    
    # @see #internal_format
    # @note pixel formats in linear colorspace
    INTERNAL_FORMATS = {
      RGB8: GL_RGB8,
      ARGB8: GL_RGBA8,
    }
    
    # @see #internal_format
    # @note pixel formats in sRGB colorspace
    INTERNAL_FORMATS_SRGB = {
      RGB8: GL_SRGB8,
      ARGB8: GL_SRGB8_ALPHA8,
    }
    
    # 'internalFormat' in glTexImage, glTexSubImage, ...
    def internal_format(as_sRGB = false)
      result = INTERNAL_FORMATS_SRGB[pixel_format] if as_sRGB
      result ||= INTERNAL_FORMATS.fetch(pixel_format)
      return result
    end
    
    # @see #format
    FORMATS = {
      RGB8: GL_BGR,
      ARGB8: GL_BGRA,
    }
    
    # 'format' in glTexImage, glTexSubImage, ...
    def format
      FORMATS.fetch(pixel_format)
    end
    
    # @see #type
    TYPES = {
      RGB8: GL_UNSIGNED_BYTE,
      ARGB8: GL_UNSIGNED_BYTE,
    }
    
    # 'type' in glTexImage, glTexSubImage, ...
    def type
      TYPES.fetch(pixel_format)
    end
    
    # Is Compressed ?
    def compressed?
      false
    end
    
  end
end
