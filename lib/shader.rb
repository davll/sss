module My
  class Shader
    
    def initialize(gl)
      self.extend gl
    end
    
    attr_reader :id
    
    def prepare(options = {})
      code = if options.key? :file
               mgr = OpenGL::Kit::ResourceGroupManager.instance
               mgr.open(options[:file]){|io| io.read }
             else
               options[:code]
             end
      @id = glCreateShader(options.fetch(:type))
      glShaderSource(@id, code)
      glCompileShader(@id)
      unless glIsShaderCompiled?(@id)
        msg = glGetShaderInfoLog(@id)
        dispose
        raise CompilationError.new("cannot compile #{options.inspect}\n => #{msg}")
      end
    end
    
    def dispose
      glDeleteShader(@id)
      @id = nil
    end
    
    class CompilationError < Exception
    end
  end
end
