require_relative 'shader'

module My
  class Program
    include OpenGL::Constants
    
    def initialize(gl)
      @gl = gl
      self.extend gl
    end
    
    attr_reader :id
    
    def prepare(options = {})
      if options.is_a? String
        mgr = OpenGL::Kit::ResourceGroupManager.instance
        options = mgr.open("#{options}.program.yml"){|io| YAML.load(io.read) }
      end
      
      shaders = options.fetch(:shaders).map do |s|
                                              o = Shader.new(@gl)
                                              o.prepare(s)
                                              o
                                            end
      @id = glCreateProgram()
      shaders.each {|s| glAttachShader(@id, s.id) }
      shaders.each {|s| s.dispose }
      
      if attribs = options[:attribs]
        attribs.each do |name, index|
          glBindAttribLocation(@id, index, name)
        end
      end
      if outputs = options[:frag_datas]
        outputs.each do |name, index|
          glBindFragDataLocation(@id, index, name)
        end
      end
      
      glLinkProgram(@id)
      unless glIsProgramLinked?(@id)
        msg = glGetProgramInfoLog(@id)
        dispose
        raise LinkerError.new("cannot link program: #{options.inspect}\n => #{msg}")
      end
    end
    
    def dispose
      glDeleteProgram(@id)
      @id = nil
    end
    
    def set_1i(name, v)
      if loc = glGetUniformLocation(@id, name)
        glUniform1i(loc, v)
      end
    end
    
    def set_1f(name, v)
      if loc = glGetUniformLocation(@id, name)
        glUniform1f(loc, v)
      end
    end
    
    def set_3f(name, vec)
      if loc = glGetUniformLocation(@id, name)
        glUniform3f(loc, vec.x, vec.y, vec.z)
      end
    end
    
    def set_4f(name, vec)
      if loc = glGetUniformLocation(@id, name)
        glUniform4f(loc, vec.x, vec.y, vec.z, vec.w)
      end
    end
    
    def set_matrix_4x4f(name, mat)
      if loc = glGetUniformLocation(@id, name)
        ::FFI::Buffer.new_in(:GLfloat, 16) do |ptr|
          ptr.write_array_of_GLfloat(mat.to_a.flatten)
          glUniformMatrix4fv(loc, 1, GL_FALSE, ptr)
        end
      end
    end
    
    class LinkerError < Exception
    end
    
  end
end
