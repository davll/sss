# -*- encoding: utf-8 -*-

module My
  class Loader 
    attr_accessor :vertices, :normals, :texcoords, :faces, :groups

    def initialize(io)
      @vertices = []
      @normals = []
      @texcoords = []
      @faces = []
      @groups = {}

      group_name = nil
      face_count = 0
      group_start_count = nil

      io.each do |line|
        next if line[0] == '#'
        values = line.split
        next if values.empty?

        case values[0]
        when 'v'
          @vertices << values[1, 3].collect { |v| v.to_f }
        when 'vn'
          @normals << values[1, 3].collect { |v| v.to_f }
        when 'vt'
          @texcoords << values[1, 2].collect { |v| v.to_f }
        when 'g'
          if group_start_count
            w = [group_start_count, face_count]
            @groups[group_name] = w
          end
          group_start_count = face_count
          group_name = values[1]
        when 'f'
          face = []
          for v in values[1..-1]
            w = v.split('/').collect { |x| x.to_i - 1 }
            face << w
          end

          if face.length == 3
            face_count += 1
            @faces << face
          elsif face.length == 4
            face_count += 2
            @faces << [face[0], face[1], face[2]]
            @faces << [face[0], face[2], face[3]]
          end
        end
      end

      if group_start_count
        w = [group_start_count, face_count]
        @groups[group_name] = w
      end

    end
  end

  class Mesh
    attr_accessor :vertex_buffer, :normal_buffer, :texcoord_buffer,
      :index_buffer, :groups

    def initialize(r_path)
      io = OpenGL::Kit::ResourceGroupManager.instance.open(r_path)
      loader = Loader.new(io)
      io.close

      @groups = loader.groups
      index_lookup = {}
      num_vertices = 0

      for face in loader.faces
        for index in face
          if not index_lookup.has_key? (index)
            index_lookup[index] = num_vertices
            num_vertices += 1
          end
        end
      end

      # build vertex buffer
      if not loader.vertices.empty?
        @vertex_buffer = Array.new(num_vertices)
        for index, real_index in index_lookup
          @vertex_buffer[real_index] = loader.vertices[index[0]]
        end
      else
        @vertex_buffer = nil
      end

      # build normal buffer
      if not loader.normals.empty?
        @normal_buffer = Array.new(num_vertices)
        for index, real_index in index_lookup
          @normal_buffer[real_index] = loader.normals[index[2]]
        end
      else
        @normal_buffer = nil
      end

      # build texcoord buffer
      if not loader.texcoords.empty?
        @texcoord_buffer = Array.new(num_vertices)
        for index, real_index in index_lookup
          @texcoord_buffer[real_index] = loader.texcoords[index[1]]
        end
      else
        @texcoord_buffer = nil
      end

      # build index buffer
      @index_buffer = Array.new(loader.faces.length)
      count = 0
      loader.faces.each_with_index do |face, index|
        @index_buffer[index] = face.collect { |x| index_lookup[x] }
      end
    end
  end

  class MeshBuffer
    include OpenGL::Constants

    def initialize(gl, mesh)
      self.extend gl
      @groups = mesh.groups

      n_buffers = 1
      if mesh.vertex_buffer then n_buffers += 1 end
      if mesh.normal_buffer then n_buffers += 1 end
      if mesh.texcoord_buffer then n_buffers += 1 end
      buffers = glGenBuffers(n_buffers)

      if mesh.vertex_buffer
        @vertex_vbo = buffers.pop

        glBindBuffer(GL_ARRAY_BUFFER, @vertex_vbo)
        glBufferData(GL_ARRAY_BUFFER,
                     mesh.vertex_buffer.length * 3 * 4, nil,
                     GL_STATIC_DRAW)
        glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
          mesh.vertex_buffer.each_with_index do |vertex, index|
            ptr.put_array_of_GLfloat(index * 12, vertex)
          end
        end
      else
        @vertex_vbo = nil
      end

      if mesh.normal_buffer
        @normal_vbo = buffers.pop

        glBindBuffer(GL_ARRAY_BUFFER, @normal_vbo)
        glBufferData(GL_ARRAY_BUFFER,
                     mesh.normal_buffer.length * 3 * 4, nil,
                     GL_STATIC_DRAW)
        glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
          mesh.normal_buffer.each_with_index do |normal, index|
            ptr.put_array_of_GLfloat(index * 12, normal)
          end
        end
      else
        @normal_vbo = nil
      end

      if mesh.texcoord_buffer
        @texcoord_vbo = buffers.pop

        glBindBuffer(GL_ARRAY_BUFFER, @texcoord_vbo)
        glBufferData(GL_ARRAY_BUFFER,
                     mesh.texcoord_buffer.length * 2 * 4, nil,
                     GL_STATIC_DRAW)
        glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
          mesh.texcoord_buffer.each_with_index do |texcoord, index|
            ptr.put_array_of_GLfloat(index * 8, texcoord)
          end
        end
      else
        @texcoord_vbo = nil
      end

      # index buffer
      @index_vbo = buffers.pop
      @index_count = mesh.index_buffer.length
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, @index_vbo)
      glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                   4 * 3 * mesh.index_buffer.length, nil,
                   GL_STATIC_DRAW)
      glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
        mesh.index_buffer.each_with_index do |index, i|
          ptr.put_array_of_GLuint(i * 12, index)
        end
      end

      # vertex array object
      @vao = glGenVertexArrays(1)[0]
      glBindVertexArray(@vao)

      # bind state
      if @vertex_vbo
        glBindBuffer(GL_ARRAY_BUFFER, @vertex_vbo)
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0)
      end

      if @normal_vbo
        glBindBuffer(GL_ARRAY_BUFFER, @normal_vbo)
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0)
      end

      if @texcoord_vbo
        glBindBuffer(GL_ARRAY_BUFFER, @texcoord_vbo)
        glEnableVertexAttribArray(2)
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0)
      end

      # index buffer
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, @index_vbo)
      
      # clear state
      glBindVertexArray(0)
      glBindBuffer(GL_ARRAY_BUFFER, 0)
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

    end

    def dispose
      glDeleteBuffers([@index_vbo, @vertex_vbo, @normal_vbo,
                       @texcoord_vbo])
      glDeleteVertexArrays([@vao])
      @index_vbo = nil
      @vertex_vbo = nil
      @normal_vbo = nil
      @texcoord_vbo = nil
      @vao = nil
    end
  
    def bind_vertex_attrib
      glBindVertexArray(@vao)
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, @index_vbo)
    end
    
    def draw
      glDrawElements(GL_TRIANGLES, @index_count * 3, GL_UNSIGNED_INT, 0)
      glBindVertexArray(0)
    end

    def draw_group(name)
      if not @groups.has_key? name
        return
      end
      glBindVertexArray(@vao)
      glDrawElements(GL_TRIANGLES,
                     (@groups[name][1] - @groups[name][0]) * 3,
                     GL_UNSIGNED_INT,
                     (@groups[name][0] * 3 * 4))
      glBindVertexArray(0)
    end

  end
end
