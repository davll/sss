module My
  class Quad
    include OpenGL::Constants
    
    def initialize(gl)
      self.extend gl
    end
    
    def prepare
      @vertex_buffer = glGenBuffers(1)[0]
      glBindBuffer(GL_ARRAY_BUFFER, @vertex_buffer)
      glBufferData(GL_ARRAY_BUFFER, 4*2*4, nil, GL_STATIC_DRAW)
      glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
        ptr.put_array_of_GLfloat   0, [-1.0, 1.0 ] # v0
        ptr.put_array_of_GLfloat   8, [-1.0,-1.0 ] # v1
        ptr.put_array_of_GLfloat  16, [ 1.0, 1.0 ] # v2
        ptr.put_array_of_GLfloat  24, [ 1.0,-1.0 ] # v3
      end
    end
    
    def dispose
      glDeleteBuffers [@vertex_buffer]
      @vertex_buffer = nil
    end
    
    def bind_vertex_attrib(vertex = 0)
      glBindBuffer(GL_ARRAY_BUFFER, @vertex_buffer)
      glEnableVertexAttribArray(vertex)
      glVertexAttribPointer(vertex, 2, GL_FLOAT, GL_FALSE, 0, 0)
    end
    
    def draw
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
    end
    
  end
end
