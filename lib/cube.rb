module My
  class Cube
    include OpenGL::Constants
    
    def initialize(gl)
      self.extend gl
    end
    
    def prepare
      # gen 2 objects
      @vertex_buffer, @index_buffer = glGenBuffers(2)
      # setup vertex buffer
      glBindBuffer(GL_ARRAY_BUFFER, @vertex_buffer)
      glBufferData(GL_ARRAY_BUFFER, 3 * 4 * 8, nil, GL_STATIC_DRAW)
      glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
        ptr.put_array_of_GLfloat( 0, [-1.0, 1.0, 1.0]) # v0
        ptr.put_array_of_GLfloat(12, [-1.0,-1.0, 1.0]) # v1
        ptr.put_array_of_GLfloat(24, [ 1.0, 1.0, 1.0]) # v2
        ptr.put_array_of_GLfloat(36, [ 1.0,-1.0, 1.0]) # v3
        ptr.put_array_of_GLfloat(48, [ 1.0, 1.0,-1.0]) # v4
        ptr.put_array_of_GLfloat(60, [ 1.0,-1.0,-1.0]) # v5
        ptr.put_array_of_GLfloat(72, [-1.0, 1.0,-1.0]) # v6
        ptr.put_array_of_GLfloat(84, [-1.0,-1.0,-1.0]) # v7
      end
      # setup index buffer
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, @index_buffer)
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * 4 * 6, nil, GL_STATIC_DRAW)
      glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY) do |ptr|
        ptr.write_array_of_GLushort [0,1,2,3,4,5,6,7,0,1,0xffff]
        ptr.put_array_of_GLushort(22, [6,0,4,2,0xffff,1,7,3,5])
        #ptr.put_array_of_GLushort( 0, [0, 1, 2, 3]) # front
        #ptr.put_array_of_GLushort( 8, [2, 3, 4, 5]) # right
        #ptr.put_array_of_GLushort(16, [4, 5, 6, 7]) # back
        #ptr.put_array_of_GLushort(24, [6, 7, 0, 1]) # left
        #ptr.put_array_of_GLushort(32, [6, 0, 4, 2]) # top
        #ptr.put_array_of_GLushort(40, [1, 7, 3, 5]) # bottom
      end
    end
    
    def dispose
      glDeleteBuffers [@vertex_buffer, @index_buffer]
      @vertex_buffer = @index_buffer = nil
    end
    
    def bind_vertex_attrib(vertex = 0)
      glBindBuffer(GL_ARRAY_BUFFER, @vertex_buffer)
      glEnableVertexAttribArray(vertex)
      glVertexAttribPointer(vertex, 3, GL_FLOAT, GL_FALSE, 0, 0)
    end
    
    def draw
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, @index_buffer)
      glEnable(GL_PRIMITIVE_RESTART)
      glPrimitiveRestartIndex(0xffff)
      glDrawElements(GL_TRIANGLE_STRIP, 20, GL_UNSIGNED_SHORT, 0)
    end
    
  end
end
