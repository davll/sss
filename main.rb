# setup environment
require 'rubygems'
require 'bundler/setup'

# load app
require_relative 'render'
require_relative 'my_app'

# Setup Resource Loading
OpenGL::Kit::ResourceGroupManager.instance.tap do |mgr|
  mgr.root = File.expand_path('../data', __FILE__)
  mgr.add_group 'default'
end

# run
MyApp.new.run
