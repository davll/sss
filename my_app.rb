require 'glut'

class MyApp < GLUT::App
  
  def initialize
    super(version: [4, 2], depth: true)
    @render = Render.new(gl_functions)
    @render[:view] = Matrix3D.translate(0, 0, -5)
    @render[:inverse_view] = Matrix3D.translate(0, 0, 5)
    @render[:world] = Matrix3D.scale(10.0, 10.0, 10.0)
    @render[:light_pos] = Vector[5.0, 5.0, -15.0, 1.0]
    @render[:light_dir] = Vector[-1, -1, 3, 0.0].normalize
    @render[:light_znear] = 1.0
    @render[:light_zfar] = 100.0
    @render[:light_cut_angle] = Math::PI * 0.25
    @render[:light_up] = Vector[0.0, 1.0, 0.0, 0.0]
    @render[:light_proj] = Matrix3D.perspective @render[:light_cut_angle], 1.0,
                                                @render[:light_znear],
                                                @render[:light_zfar]
    @render[:b_scatter] = true
    @render[:b_light] = true
    @render[:scatter_strench] = 0.1
  end
  
  def display
    @render.draw
    super
  end
  
  def cleanup
    puts "closed"
  end
  
  def reshape(width, height)
    glViewport(0, 0, width, height)
    @render[:window_width] = width
    @render[:window_height] = height
    
    fovy = 45.0 / 180.0 * Math::PI
    aspect = Float(width) / Float(height)
    z_near, z_far = 1.0, 100.0
    @render[:projection] = Matrix3D.perspective(fovy, aspect, z_near, z_far)
  end
  
  def mouse(button, button_state, x, y)
    if button == :LEFT
      case button_state
      when :DOWN
        @left_mouse_pos = [x, y]
      when :UP
        remove_instance_variable(:@left_mouse_pos)
      end
    end
    if button == :RIGHT
      case button_state
      when :DOWN
        @right_mouse_pos = [x, y]
      when :UP
        remove_instance_variable(:@right_mouse_pos)
      end
    end
  end
  
  def motion(x, y)
    if defined? @left_mouse_pos
      delta_x = (x - @left_mouse_pos[0]) * 0.01
      delta_y = (y - @left_mouse_pos[1]) * 0.01
      
      rotx = Matrix3D.rotate(delta_y, 1.0, 0.0, 0.0)
      roty = Matrix3D.rotate(delta_x, 0.0, 1.0, 0.0)
      @render[:world] = rotx * roty * @render[:world]
      
      @left_mouse_pos = [x, y]
      glutPostRedisplay()
    end
    if defined? @right_mouse_pos
      delta_x = (x - @right_mouse_pos[0]) * 0.001
      delta_y = (y - @right_mouse_pos[1]) * 0.001
      
      rotx = Matrix3D.rotate(delta_y, 1.0, 0.0, 0.0)
      roty = Matrix3D.rotate(delta_x, 0.0, 1.0, 0.0)
      r = rotx * roty
      lpos = rotx * roty * @render[:light_pos]
      @render[:light_dir] = @render[:light_dir]
      @render[:light_pos] = r * @render[:light_pos]
      @render[:light_up] = r * @render[:light_up]
      
      glutPostRedisplay()
    end
  end
  
  def keyboard(key, x, y)
    case key.chr
    when 's'
      @render[:b_scatter] = !@render[:b_scatter]
      glutPostRedisplay()
    when 'l'
      @render[:b_light] = !@render[:b_light]
      glutPostRedisplay()
    when '1'
      @render[:scatter_strench] += 1.0
      puts @render[:scatter_strench]
      glutPostRedisplay()
    when '2'
      @render[:scatter_strench] -= 1.0
      puts @render[:scatter_strench]
      glutPostRedisplay()
    end
  end
  
end
