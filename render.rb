require 'opengl/kit'
require_relative 'lib/cube'
require_relative 'lib/quad'
require_relative 'lib/shader'
require_relative 'lib/program'
require_relative 'lib/obj_loader'
require_relative 'lib/texture'

class Render
  include OpenGL::Constants
  include OpenGL::Kit
  
  def initialize(gl)
    self.extend gl
    @parameters = {}
    
    @quad = My::Quad.new(gl).tap do |g| g.prepare end
    # @geom = My::Cube.new(gl).tap do |g| g.prepare end
    @geom = My::MeshBuffer.new(gl, My::Mesh.new('head.obj'))
    
    @skin_tex = My::Texture.load gl, 'head-albedo.tga'
    @normal_tex = My::Texture.load gl, 'head-normal-object.tga'
    
    @program_lighting = My::Program.new(gl).tap do |program|
      program.prepare 'plastic'
    end
    @program_scattering = My::Program.new(gl).tap do |program|
      program.prepare 'scattering'
    end
    @program_shadow = My::Program.new(gl).tap do |program|
      program.prepare 'shadow_map'
    end
    
    @shadow_map_tex = glGenTextures(1)[0]
    @shadow_map_fbo = glGenFramebuffers(1)[0]
    @shadow_map_rbo = glGenRenderbuffers(1)[0]
    @shadow_map_size = 1024
    
    glBindTexture GL_TEXTURE_2D, @shadow_map_tex
    glTexImage2D GL_TEXTURE_2D, 0, GL_R32F, 1024, 1024,
                 0, GL_RED, GL_FLOAT, nil
    #glTexParameterf GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR
    glBindRenderbuffer GL_RENDERBUFFER, @shadow_map_rbo
    glRenderbufferStorage GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, 1024, 1024
    glBindFramebuffer GL_FRAMEBUFFER, @shadow_map_fbo
    glFramebufferTexture2D GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, @shadow_map_tex, 0
    glFramebufferRenderbuffer GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, @shadow_map_rbo
    puts glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE
  end
  
  def [](key)
    @parameters.fetch(key)
  end
  
  def []=(key, value)
    @parameters.store(key, value)
  end
  
  def update_uniform(prog)
    prog.set_matrix_4x4f 'u_Projection', self[:projection]
    prog.set_matrix_4x4f 'u_ModelView', self[:view] * self[:world]
    prog.set_3f 'u_Albedo', Vector[0.4, 0.4, 0.4]
    prog.set_3f 'u_Ks', Vector[0.5, 0.5, 0.5]
    prog.set_1f 'u_Ns', 2.0
    prog.set_3f 'u_Light_Ka', Vector[0.4, 0.4, 0.4]
    prog.set_3f 'u_Light_Kd', Vector[1.0, 1.0, 1.0]
    prog.set_3f 'u_Light_Ks', Vector[0.5, 0.5, 0.5]
    prog.set_3f 'u_Light_Pos', (self[:view] * self[:light_pos])
    prog.set_3f 'u_Light_SpotDir', (self[:view] * self[:light_dir]).normalize
    prog.set_1f 'u_Light_SpotCosCutoff', Math.cos(self[:light_cut_angle])
    prog.set_1f 'u_SSS_Strength', self[:scatter_strench]
    prog.set_1i 's_ShadowMap', 0
    prog.set_1i 's_AlbedoMap', 1
    prog.set_1i 's_NormalMap', 2
  end
  
  def draw
    light_lookat = Matrix3D.look_at self[:light_pos].xyz,
                                    self[:light_pos].xyz + self[:light_dir].xyz,
                                    self[:light_up].xyz
    
    glEnable GL_DEPTH_TEST
    glEnable GL_CULL_FACE
    
    # RenderTarget: Shadow FBO
    glBindFramebuffer GL_DRAW_FRAMEBUFFER, @shadow_map_fbo
    glDrawBuffer GL_COLOR_ATTACHMENT0
    glViewport(0, 0, @shadow_map_size, @shadow_map_size)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    
    glDepthFunc GL_LESS
    glDisable GL_BLEND
    
    @program_shadow.tap do |prog|
      glUseProgram(prog.id)
      update_uniform(prog)
      prog.set_matrix_4x4f 'u_Projection', self[:light_proj]
      prog.set_matrix_4x4f 'u_ModelView', light_lookat * self[:world]
    end
    
    @geom.bind_vertex_attrib
    @geom.draw
    
    # Bind Texture
    glActiveTexture GL_TEXTURE0
    glBindTexture GL_TEXTURE_2D, @shadow_map_tex
    glGenerateMipmap GL_TEXTURE_2D
    glActiveTexture GL_TEXTURE1
    glBindTexture GL_TEXTURE_2D, @skin_tex.object
    glActiveTexture GL_TEXTURE2
    glBindTexture GL_TEXTURE_2D, @normal_tex.object
    
    # RenderTarget: Back Buffer
    glBindFramebuffer GL_DRAW_FRAMEBUFFER, 0
    glDrawBuffer GL_BACK
    glViewport(0, 0, self[:window_width], self[:window_height])
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    
    # debug draw
    if false
      glUseProgram @program_debug.id
      @quad.bind_vertex_attrib
      @quad.draw
      return
    end
    
    if self[:b_light]
      @program_lighting.tap do |prog|
        glUseProgram(prog.id)
        update_uniform(prog)
      end
      @geom.bind_vertex_attrib 
      @geom.draw
    end
    
    glDepthFunc GL_LEQUAL
    glEnable GL_BLEND
    glBlendFunc GL_ONE, GL_ONE
    
    if self[:b_scatter]
      @program_scattering.tap do |prog|
        glUseProgram(prog.id)
        update_uniform(prog)
        light_mat = self[:light_proj] * light_lookat * self[:inverse_view]
        prog.set_matrix_4x4f 'u_LightFromView', light_mat
      end
      @geom.bind_vertex_attrib 
      @geom.draw
    end
  end
  
end
