#version 150 core
uniform mat4 u_ModelView;
uniform sampler2D s_NormalMap;
uniform vec3 u_Albedo;
uniform vec3 u_Light_Kd;
uniform vec3 u_Light_Pos;
uniform vec3 u_Light_SpotDir;
uniform float u_Light_SpotCosCutoff;
uniform mat4 u_LightFromView;
uniform sampler2D s_ShadowMap;
uniform float u_SSS_Strength;

in vec3 gs_Position;
//in vec3 gs_Normal;
in vec2 gs_TexCoord;
out vec4 fs_FragColor;

// distance
float s_distance(vec3 posV, vec3 normalV) {
  // Shrink the position to avoid artifacts on the silhouette
  posV = posV - 0.01 * normalV;
  // Transform to light space (from view space)
  vec4 posL = u_LightFromView * vec4(posV, 1.0);
  posL.xy = posL.xy / posL.w * 0.5 + 0.5;
  // Fetch depth from the shadow map
  float d1 = texture(s_ShadowMap, posL.xy).r;
  float d2 = -posL.w;
  // Calculate the difference
  return abs(d1 - d2);
} // END distance

vec3 T(float s) {
  return vec3(0.233, 0.455, 0.648) * exp(-s * s / 0.0064) +
         vec3(0.1,   0.336, 0.344) * exp(-s * s / 0.0484) +
         vec3(0.118, 0.198, 0.0)   * exp(-s * s / 0.187)  +
         vec3(0.113, 0.007, 0.007) * exp(-s * s / 0.567)  +
         vec3(0.358, 0.004, 0.0)   * exp(-s * s / 1.99)   +
         vec3(0.078, 0.0,   0.0)   * exp(-s * s / 7.41);
} // END T

void main()
{ // BEGIN
  
  // Read Normal
  vec3 gs_Normal = texture(s_NormalMap, gs_TexCoord).xyz * 2.0 - 1.0;
  gs_Normal = normalize((u_ModelView * vec4(gs_Normal, 0.0)).xyz);
  
  // Calculate the distance traveled by the light inside of the object
  float s = s_distance(gs_Position, gs_Normal) / u_SSS_Strength;
  
  // Estimate the irradiance on the back
  vec3 light = normalize(u_Light_Pos - gs_Position); // regular light vec
  float irradiance = max(0.3 + dot(-gs_Normal, light), 0.0);
  
  // Light Vector (from point to light)
  vec3 lv = normalize(u_Light_Pos - gs_Position);
  
  // Spot cutoff
  float cut = dot(lv, -u_Light_SpotDir) - u_Light_SpotCosCutoff;
  cut = max(0.0, sign(cut));
  
  // Calculate transmitted light
  vec3 transmittance = T(s) * u_Albedo * u_Light_Kd * irradiance * cut;
  
  // Final
  vec3 color = transmittance;
  fs_FragColor = vec4(color, 1.0);
} // END
