#version 150 core

uniform mat4 u_ModelView;
uniform sampler2D s_AlbedoMap;
uniform sampler2D s_NormalMap;
uniform vec3 u_Ks;
uniform float u_Ns;
uniform vec3 u_Light_Ka;
uniform vec3 u_Light_Kd;
uniform vec3 u_Light_Ks;
uniform vec3 u_Light_Pos;
uniform vec3 u_Light_SpotDir;
uniform float u_Light_SpotCosCutoff;

in vec3 gs_Position;
//in vec3 gs_Normal;
in vec2 gs_TexCoord;
out vec4 fs_FragColor;

void main()
{ // BEGIN
  // Light Vector (from point to light)
  vec3 lv = normalize(u_Light_Pos - gs_Position);
  // Half Vector (from point)
  vec3 hv = normalize(lv + vec3(0,0,1));
  
  // Read Albedo
  vec3 albedo = texture(s_AlbedoMap, gs_TexCoord).xyz;
  // Read Normal
  vec3 gs_Normal = texture(s_NormalMap, gs_TexCoord).xyz * 2.0 - 1.0;
  gs_Normal = normalize((u_ModelView * vec4(gs_Normal, 0.0)).xyz);
  
  // Ambient
  vec3 ka = albedo * u_Light_Ka;
  // Diffuse
  vec3 kd = albedo * u_Light_Kd;
  kd *= max(0, dot(gs_Normal, lv));
  // Specular
  vec3 ks = u_Ks * u_Light_Ks;
  ks *= pow(max(0, dot(gs_Normal, hv)), u_Ns);
  
  // Spot cutoff
  float cut = dot(lv, -u_Light_SpotDir) - u_Light_SpotCosCutoff;
  cut = max(0.0, sign(cut));
  kd *= cut; ks *= cut;
  
  // Final
  vec3 color = ka + kd + ks;
  fs_FragColor = vec4(color, 1.0);
} // END
