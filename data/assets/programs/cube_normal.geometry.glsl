# version 150 core
uniform mat4 u_Projection;
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;
out vec3 gs_Position; // view space
out vec3 gs_Normal; // view space
void main()
{ // BEGIN
  vec3 v[3];
  for (int i = 0; i < 3; ++i)
    v[i] = gl_in[i].gl_Position.xyz;
  vec3 normal = normalize(cross(v[1]-v[0], v[2]-v[0]));
  for (int i = 0; i < 3; ++i) {
    gl_Position = u_Projection * vec4(v[i], 1.0);
    gs_Position = v[i];
    gs_Normal = normal;
    EmitVertex();
  } // END FOR
  EndPrimitive();
} // END
