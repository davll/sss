#version 150 core

uniform mat4 u_Projection;
uniform mat4 u_ModelView;

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord;

out vec3 gs_Position;
out vec3 gs_Normal;
out vec2 gs_TexCoord;

void main()
{ // BEGIN
  vec4 pos = u_ModelView * in_Position;
  gl_Position = u_Projection * pos;
  gs_Position = pos.xyz;
  gs_Normal = normalize((u_ModelView * vec4(in_Normal, 0.0)).xyz);
  gs_TexCoord = in_TexCoord;
} // END
