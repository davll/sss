#version 150 core
uniform mat4 u_Projection;
in vec3 gs_Position;
in vec3 gs_Normal;
out vec4 fs_FragColor;
void main()
{ // BEGIN
  vec4 pos = u_Projection * vec4(gs_Position, 1.0);
  fs_FragColor = vec4(-pos.w);
} // END
